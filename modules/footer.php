<footer class="clearfix">
	<span class="copyright">
		&#169;
		<?php
		define('FIRST_YEAR', '2018');
		echo FIRST_YEAR;
		if(FIRST_YEAR != date('Y')) {
			echo '-'.date("Y");
		}
		?>
		All Rights Reserved Terms of Use and Privacy Policy
	</span>
</footer>
</body>
</html>
