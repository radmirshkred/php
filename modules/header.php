<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Beauty salon</title>
	<link href="../css/normalize.css" rel="stylesheet">
	<link href="../css/style.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,700,700i,900,900i" rel="stylesheet">

</head>
<body>
<header class="clearfix">
	<div class="header-logo">
		<img src="../images/logo.jpeg" alt="Логотип">
	</div>
	<div class="header-top clearfix">
		<div class="header-top-first clearfix ">
			<div class="header-top-first-img sprite3 sprite3-phone"></div>
			<div class="header-top-first-paragr">
				<p>1-800-1234-567<br>
					1-800-8763-098</p>
			</div>
		</div>
		<div class="header-top-second clearfix">
			<div class="header-top-second-img sprite3 sprite3-mail"></div>
			<div class="header-top-second-paragr">
				<p>Mon-Fri: 9:00am-8:00pm<br>
					Sat-Sun: 9:00am-3:00pm</p>
			</div>
		</div>
		<div class="header-top-third clearfix">
			<div class="header-top-third-img sprite3 sprite3-location"></div>
			<div class="header-top-third-paragr">
				<p>2130 Fulton Street, San Diego,<br>
					CA 94117-1080 USA</p>
			</div>
		</div>
		<div class="header-top-fourth">
			<a href="#" class="header-top-fourth-img sprite sprite-button-1"></a>
		</div>
	</div>
	<nav class="clearfix">
		<div><a href="../index.php?page=main" class="nav-home">Home</a></div>
		<div class="nav-container-aboutus clearfix">
			<a href="../index.php?module=static&page=about_us" class="nav-aboutus">About Us</a>
			<div class="nav-aboutus-pointer sprite3 sprite3-pointer-down"></div>
		</div>
		<div class="nav-container-services clearfix">
			<a href="../index.php?page=services" class="nav-services">Services</a>
			<div class="nav-services-pointer sprite3 sprite3-pointer-down"></div>
		</div>
		<div><a href="../index.php?page=locations" class="nav-locations">Locations</a></div>
		<div class="nav-container-blog clearfix">
			<a href="../index.php?page=blog" class="nav-blog">Blog</a>
			<div class="nav-blog-pointer sprite3 sprite3-pointer-down"></div>
		</div>
		<div class="nav-container-pages clearfix">
			<a href="../index.php?page=pages" class="nav-pages">Pages</a>
			<div class="nav-pages-pointer sprite3 sprite3-pointer-down"></div>
		</div>
		<div><a href="../index.php?page=contacts" class="nav-contacts">Contacts</a></div>
		<?php
		if($_SERVER['REMOTE_ADDR'] == '127.0.0.1' or $_SERVER['REMOTE_ADDR'] == '176.59.35.145') {
			echo '<div><a href="../modules/static/data.php" class="nav-contacts">Admin</a></div>';
		}
		?>
		<div class="nav-search">
			<a href="#" class="sprite3 sprite3-search"></a>
		</div>
	</nav>
</header>