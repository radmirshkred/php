<main class="clearfix">
	<div class="clearfix girl-container">
		<div class="girl-container-info clearfix">
			<div class="girl-container-info-text">
				<p class="girl-paragr-1">Call Now!</p>
				<p class="girl-paragr-2">1-800-1234-567</p>
				<p class="girl-paragr-3">Our experienced hair stylists and top beauticians
					are dedicated to make you look your best!</p>
			</div>
			<div class="girl-container-info-button">
				<a href="#" class="sprite sprite-button-2"></a>
			</div>
			<div class="girl-container-info-pointers">
				<a href="#" class="sprite3 sprite3-pointer-left"></a>
				<a href="#" class="sprite3 sprite3-pointer-right"></a>
			</div>
		</div>
	</div>
	<div class="our-services clearfix">
		<div class="our-services-container clearfix">
			<p class="our-services-offer">What We Offer</p>
			<p class="our-services-title">Our Services</p>
			<div class="sprite3 sprite3-red-stripe"></div>
			<p class="our-services-text">Whether you're looking to threat youself to a facial or celebrating a special
				occasion, aromatherapy, our beauty and skin care services will suit every beauty need.</p>
			<div class="our-services-img clearfix">
				<div class="our-services-img-1">
					<div class="sprite2 sprite2-pic-1"></div>
					<span>Facial Treatments</span>
				</div>
				<div class="our-services-img-2">
					<div class="sprite2 sprite2-pic-2"></div>
					<span>Makeup</span>
				</div>
				<div class="our-services-img-3">
					<div class="sprite2 sprite2-pic-3"></div>
					<span>Nail Care Services</span>
				</div>
				<div class="our-services-img-4">
					<div class="sprite2 sprite2-pic-4"></div>
					<span>Hair Care Services</span>
				</div>
			</div>
			<div class="our-services-button clearfix">
				<a href="#" class="sprite sprite-button-3"></a>
			</div>
		</div>
	</div>
	<div class="pricing clearfix">
		<div class="pricing-container clearfix">
			<p class="pricing-packages">Our packages</p>
			<p class="pricing-title">Pricing</p>
			<div class="sprite3 sprite3-red-stripe-2"></div>
			<div class="pricing-offer-container clearfix">
				<div class="pricing-offer-1">
					<span class="pricing-offer-title">Basic</span>
					<div class="offer-text-container">
						<span class="pricing-offer-text">Hair Extension</span>
						<span class="pricing-offer-text">Makeup Application</span>
						<span class="pricing-offer-text grey-text">Facial</span>
						<span class="pricing-offer-text grey-text">Nails</span>
						<span class="pricing-offer-text grey-text">Styling</span>
					</div>
					<span class="pricing-offer-price"><sup class="bucks">$</sup>99.00</span>
					<div class="pricing-button">
						<a href="#" class="sprite sprite-button-4"></a>
					</div>
				</div>
				<div class="pricing-offer-2">
					<span class="pricing-offer-title">Classic</span>
					<div class="offer-text-container">
						<span class="pricing-offer-text">Hair Extension</span>
						<span class="pricing-offer-text">Makeup Application</span>
						<span class="pricing-offer-text">Facial</span>
						<span class="pricing-offer-text grey-text">Nails</span>
						<span class="pricing-offer-text grey-text">Styling</span>
					</div>
					<span class="pricing-offer-price"><sup class="bucks">$</sup>149.00</span>
					<div class="pricing-button">
						<a href="#" class="sprite sprite-button-4"></a>
					</div>
				</div>
				<div class="pricing-offer-3">
					<span class="pricing-offer-title-a">Gold</span>
					<div class="offer-text-container">
						<span class="pricing-offer-text">Hair Extension</span>
						<span class="pricing-offer-text">Makeup Application</span>
						<span class="pricing-offer-text">Facial</span>
						<span class="pricing-offer-text">Nails</span>
						<span class="pricing-offer-text grey-text">Styling</span>
					</div>
					<span class="pricing-offer-price"><sup class="bucks">$</sup>199.00</span>
					<div class="pricing-button">
						<a href="#" class="sprite sprite-button-4-a"></a>
					</div>
					<img src="../../images/most-popular.png" alt="most-popular" class="most-popular">
				</div>
				<div class="pricing-offer-4">
					<span class="pricing-offer-title">Platinum</span>
					<div class="offer-text-container">
						<span class="pricing-offer-text">Hair Extension</span>
						<span class="pricing-offer-text">Makeup Application</span>
						<span class="pricing-offer-text">Facial</span>
						<span class="pricing-offer-text">Nails</span>
						<span class="pricing-offer-text">Styling</span>
					</div>
					<span class="pricing-offer-price"><sup class="bucks">$</sup>249.00</span>
					<div class="pricing-button">
						<a href="#" class="sprite sprite-button-4"></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>
