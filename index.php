<?php
include 'tunes.php';

include_once './config.php';
include_once './libs/default.php';
include_once './variables.php';
include './modules/header.php';
if(!isset($_GET['page'])) {
	$_GET['page'] = 'main';
}
include './modules/'.$_GET['module'].'/'.$_GET['page'].'.php';
include './skins/'.SKIN.'/static/'.$_GET['page'].'.tpl';
include './modules/footer.php';
